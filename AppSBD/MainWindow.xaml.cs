using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
using System.Data.SqlClient;

namespace AppSBD
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        SqlConnection con = new SqlConnection("Data Source=(local);Initial Catalog=SistemInformasiPendataanTempatWisata;Integrated Security=True");
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;
        private void clear()
        {
            KodeOW.Text = " ";
            NamaOW.Text = " ";
            LokasiOW.Text = " ";
            WaktuOP.Text = " ";
            Deskripsi.Text = " ";
        }
        private void show()
        {
            da = new SqlDataAdapter("SELECT * FROM Objek_Wisata", con);
            dt = new DataTable();
            da.Fill(dt);
            ViewOW.ItemsSource = dt.DefaultView;
            KodeOW.Focus();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            show();
        }

        private void InsertOW_Click(object sender, RoutedEventArgs e)
        {
            cmd = new SqlCommand("INSERT INTO Objek_Wisata VALUES (@kodeow, @nama, @lokasi, @deskripsi, @waktu_operasional)", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@kodeow", KodeOW.Text);
            cmd.Parameters.AddWithValue("@nama", NamaOW.Text);
            cmd.Parameters.AddWithValue("@lokasi", LokasiOW.Text);
            cmd.Parameters.AddWithValue("@deskripsi", Deskripsi.Text);
            cmd.Parameters.AddWithValue("@waktu_operasional", WaktuOP.Text);
            
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            show();
            clear();
            /*SqlCommand cmd = new SqlCommand("select userid, password from user where user id = @id and password = @password", con);

            cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = txtUsername.Text;
            cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = txtPassword.Text;
            cmd = new SqlCommand("SELECT Kode_OW, Nama, Lokasi, Waktu_Operasional, Deskrpsi" +
                                 "FROM Objek_Wisata WHERE Kode_OW=@kode AND Nama=@nama AND Lokasi=@lokasi AND" +
                                 "Waktu_Operasional=@waktu AND Deskrpsi=deskp", con);*/
           
        }

        private void UpdateOW_Click(object sender, RoutedEventArgs e)
        {
            cmd = new SqlCommand("UPDATE Objek_Wisata SET Nama=@nama, Lokasi=@lokasi, Waktu_Operasional=@waktu_operasional, Deskripsi=@deskripsi" +
                "                WHERE KodeOW=@kodeow", con);
            cmd.Parameters.AddWithValue("@kodeow", KodeOW.Text);
            cmd.Parameters.AddWithValue("@nama", NamaOW.Text);
            cmd.Parameters.AddWithValue("@lokasi", LokasiOW.Text);
            cmd.Parameters.AddWithValue("@waktu_operasional", WaktuOP.Text);
            cmd.Parameters.AddWithValue("@deskripsi", Deskripsi.Text);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            show();
            clear();
        }

        private void DeleteOW_Click(object sender, RoutedEventArgs e)
        {
            cmd = new SqlCommand("DELETE FROM Objek_Wisata WHERE KodeOW=@kodeow", con);
            cmd.Parameters.AddWithValue("@kodeow", KodeOW.Text);
            cmd.Parameters.AddWithValue("@nama", NamaOW.Text);
            cmd.Parameters.AddWithValue("@lokasi", LokasiOW.Text);
            cmd.Parameters.AddWithValue("@waktu_operasional", WaktuOP.Text);
            cmd.Parameters.AddWithValue("@deskrpsi", Deskripsi.Text);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            show();
            clear();
        }

        private void ViewOW_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Show();
            /*try
            {
                if(ViewOW.Items.Count > 0)
                {
                    KodeOW.Text = ((DataRowView)ViewOW.SelectedItem).Row["Kode_OW"].ToString();
                    NamaOW.Text = ((DataRowView)ViewOW.SelectedItem).Row["Nama"].ToString();
                    LokasiOW.Text = ((DataRowView)ViewOW.SelectedItem).Row["Lokasi"].ToString();
                    WaktuOP.Text = ((DataRowView)ViewOW.SelectedItem).Row["Waktu_Operasional"].ToString();
                    Deskripsi.Text = ((DataRowView)ViewOW.SelectedItem).Row["Deskrpsi"].ToString();
                }
            }
            catch(Exception ex)
            { }*/
        }
    }
}
