using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using System.Data.SqlClient;

namespace AppSBD
{
    /// <summary>
    /// Interaction logic for PenjualanTiket.xaml
    /// </summary>
    public partial class PenjualanTiket : Window
    {
        public PenjualanTiket()
        {
            InitializeComponent();
        }

        SqlConnection con = new SqlConnection("Data Source=(local);Initial Catalog=SistemInformasiPendataanTempatWisata;Integrated Security=True");
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;
        private void clear()
        {
            NT.Text = " ";
            HariPt.Text = " ";
            TanggalPt.Text = " ";
            NIPPt.Text = " ";
            KOPt.Text = " ";
        }
        private void show()
        {
            da = new SqlDataAdapter("SELECT a.Nomer_Tiket, a.Hari, a.Tanggal, a.NIP, c.Nama AS Karyawan, a.KodeOW, b.Nama AS Objek_Wisata FROM Penjualan_Tiket AS a INNER JOIN Objek_Wisata AS b ON a.KodeOW = b.KodeOW INNER JOIN Karyawan AS c ON a.NIP = c.NIP", con);
            dt = new DataTable();
            da.Fill(dt);
            ViewPt.ItemsSource = dt.DefaultView;
            NT.Focus();
        }
        
        private void InsertPj_Click(object sender, RoutedEventArgs e)
        {
            cmd = new SqlCommand("INSERT INTO Penjualan_Tiket VALUES (@nomer_tiket, @hari, @tanggal, @nip, @kodeow)", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@nomer_tiket", NT.Text);
            cmd.Parameters.AddWithValue("@hari", HariPt.Text);
            cmd.Parameters.AddWithValue("@tanggal", TanggalPt.Text);
            cmd.Parameters.AddWithValue("@nip", NIPPt.Text);
            cmd.Parameters.AddWithValue("@kodeow", KOPt.Text);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            show();
            clear();
        }

        private void UpdatePj_Click(object sender, RoutedEventArgs e)
        {
            cmd = new SqlCommand("UPDATE Penjualan_Tiket SET Hari=@hari, Tanggal=@tanggal WHERE Nomer_Tiket=@nomer_tiket", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@nomer_tiket", NT.Text);
            cmd.Parameters.AddWithValue("@hari", HariPt.Text);
            cmd.Parameters.AddWithValue("@tanggal", TanggalPt.Text);
            cmd.Parameters.AddWithValue("@nip", NIPPt.Text);
            cmd.Parameters.AddWithValue("@kodeow", KOPt.Text);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            show();
            clear();
        }

        private void DeletePj_Click(object sender, RoutedEventArgs e)
        {
            cmd = new SqlCommand("DELETE FROM Penjualan_Tiket WHERE Nomer_Tiket=@nomer_tiket", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@nomer_tiket", NT.Text);
            cmd.Parameters.AddWithValue("@hari", HariPt.Text);
            cmd.Parameters.AddWithValue("@tanggal", TanggalPt.Text);
            cmd.Parameters.AddWithValue("@nip", NIPPt.Text);
            cmd.Parameters.AddWithValue("@kodeow", KOPt.Text);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            show();
            clear();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            show();
        }
    }
}
