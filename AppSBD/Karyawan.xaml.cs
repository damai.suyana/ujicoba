using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using System.Data.SqlClient;

namespace AppSBD
{
    /// <summary>
    /// Interaction logic for Karyawan.xaml
    /// </summary>
    public partial class Karyawan : Window
    {
        public Karyawan()
        {
            InitializeComponent();
        }

        SqlConnection con = new SqlConnection("Data Source=(local);Initial Catalog=SistemInformasiPendataanTempatWisata;Integrated Security=True");
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;
        private void clear()
        {
            NIP.Text = " ";
            Namakr.Text = " ";
            Alamat.Text = " ";
            JK.Text = " ";
            Profesi.Text = " ";
        }
        private void show()
        {
            da = new SqlDataAdapter("SELECT * FROM Karyawan", con);
            dt = new DataTable();
            da.Fill(dt);
            ViewKr.ItemsSource = dt.DefaultView;
            NIP.Focus();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            show();
        }
        
        private void InsertKr_Click(object sender, RoutedEventArgs e)
        {
            cmd = new SqlCommand("INSERT INTO Karyawan VALUES (@nip, @nama, @alamat, @gender, @profesi)", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@nip", NIP.Text);
            cmd.Parameters.AddWithValue("@nama", Namakr.Text);
            cmd.Parameters.AddWithValue("@alamat", Alamat.Text);
            cmd.Parameters.AddWithValue("@gender", JK.Text);
            cmd.Parameters.AddWithValue("@profesi", Profesi.Text);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            show();
            clear();
        } 

        private void UpdateKr_Click(object sender, RoutedEventArgs e)
        {
            cmd = new SqlCommand("UPDATE Karyawan SET Nama=@nama, Alamat=@alamat, Gender=@gender, Profesi=@profesi WHERE NIP=@nip", con);
            cmd.Parameters.AddWithValue("@nip", NIP.Text);
            cmd.Parameters.AddWithValue("@nama", Namakr.Text);
            cmd.Parameters.AddWithValue("@alamat", Alamat.Text);
            cmd.Parameters.AddWithValue("@gender", JK.Text);
            cmd.Parameters.AddWithValue("@profesi", Profesi.Text);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            show();
            clear();
        }

        private void DeleteKr_Click(object sender, RoutedEventArgs e)
        {
            cmd = new SqlCommand("DELETE FROM Karyawan WHERE NIP=@nip", con);
            cmd.Parameters.AddWithValue("@nip", NIP.Text);
            cmd.Parameters.AddWithValue("@nama", Namakr.Text);
            cmd.Parameters.AddWithValue("@alamat", Alamat.Text);
            cmd.Parameters.AddWithValue("@gender", JK.Text);
            cmd.Parameters.AddWithValue("@profesi", Profesi.Text);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            show();
            clear();
        } 
    }
}
