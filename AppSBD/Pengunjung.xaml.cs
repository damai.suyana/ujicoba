using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using System.Data.SqlClient;

namespace AppSBD
{
    /// <summary>
    /// Interaction logic for Pengunjung.xaml
    /// </summary>
    public partial class Pengunjung : Window
    {
        public Pengunjung()
        {
            InitializeComponent();
        }

        SqlConnection con = new SqlConnection("Data Source=(local);Initial Catalog=SistemInformasiPendataanTempatWisata;Integrated Security=True");
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;
        private void clear()
        {
            IDPj.Text = " ";
            NamaPj.Text = " ";
            JKPj.Text = " ";
            NtPj.Text = " ";
        }
        private void show()
        {
            da = new SqlDataAdapter("SELECT * FROM Pengunjung", con);
            dt = new DataTable();
            da.Fill(dt);
            ViewPj.ItemsSource = dt.DefaultView;
            IDPj.Focus();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            show();
        }

        private void InsertPj_Click(object sender, RoutedEventArgs e)
        {
            cmd = new SqlCommand("INSERT INTO Pengunjung VALUES (@id_pengunjung, @nama, @gender, @nomer_tiket)", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@id_pengunjung", IDPj.Text);
            cmd.Parameters.AddWithValue("@nama", NamaPj.Text);
            cmd.Parameters.AddWithValue("@gender", JKPj.Text);
            cmd.Parameters.AddWithValue("@nomer_tiket", NtPj.Text);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            show();
            clear();
        }

        private void UpdatePj_Click(object sender, RoutedEventArgs e)
        {
            cmd = new SqlCommand("UPDATE Pengunjung SET Nama=@nama, Gender=@gender, Nomer_Tiket=@nomer_tiket WHERE ID_Pengunjung=@id_pengunjung", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@id_pengunjung", IDPj.Text);
            cmd.Parameters.AddWithValue("@nama", NamaPj.Text);
            cmd.Parameters.AddWithValue("@gender", JKPj.Text);
            cmd.Parameters.AddWithValue("@nomer_tiket", NtPj.Text);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            show();
            clear();
        }

        private void DeletePj_Click(object sender, RoutedEventArgs e)
        {
            cmd = new SqlCommand("DELETE FROM Pengunjung WHERE ID_Pengunjung=@id_pengunjung", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@id_pengunjung", IDPj.Text);
            cmd.Parameters.AddWithValue("@nama", NamaPj.Text);
            cmd.Parameters.AddWithValue("@gender", JKPj.Text);
            cmd.Parameters.AddWithValue("@nomer_tiket", NtPj.Text);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            show();
            clear();
        }
    }
}
