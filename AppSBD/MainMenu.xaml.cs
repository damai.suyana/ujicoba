using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AppSBD
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void buttonOW_Click(object sender, RoutedEventArgs e)
        {
            MainWindow OW = new MainWindow();
            OW.Show();
        }

        private void ButtonKR_Click(object sender, RoutedEventArgs e)
        {
            Karyawan KR = new Karyawan();
            KR.Show();
        }

        private void ButtonPJ_Click(object sender, RoutedEventArgs e)
        {
            Pengunjung PJ = new Pengunjung();
            PJ.Show();
        }

        private void ButtonPT_Click(object sender, RoutedEventArgs e)
        {
            PenjualanTiket PT = new PenjualanTiket();
            PT.Show();
        }
    }
}
